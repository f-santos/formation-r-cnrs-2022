#+TITLE: Formation CNRS : Initiation à R
#+SUBTITLE: Une trame d'analyse du "Goldman Data Set"
#+AUTHOR: Frédéric Santos
#+DATE: 5--6 septembre 2022
#+EMAIL: frederic.santos@u-bordeaux.fr 
#+STARTUP: showall
#+OPTIONS: email:t toc:nil
#+LATEX_HEADER: \usepackage[natbibapa]{apacite}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{mathpazo}
#+LATEX_HEADER: \usepackage{amsmath}
#+LATEX_HEADER: \usepackage{titlesec}
#+LATEX_HEADER: \titlelabel{\thetitle.\quad}
#+LATEX_HEADER: \usepackage[usenames,dvipsnames]{xcolor} % For colors with friendly names
#+LATEX_HEADER: \usepackage{minted}
#+LATEX_HEADER: \usepackage{mdframed}                    % Companion of minted for code blocks
#+LATEX_HEADER: \usepackage{fancyvrb}                    % For verbatim R outputs
#+LATEX_HEADER: \usemintedstyle{friendly} % set style if needed, see https://frama.link/jfRr8Lpj
#+LATEX_HEADER: \mdfdefinestyle{mystyle}{linecolor=gray!30,backgroundcolor=gray!30}
#+LATEX_HEADER: \BeforeBeginEnvironment{minted}{%
#+LATEX_HEADER: \begin{mdframed}[style=mystyle]}
#+LATEX_HEADER: \AfterEndEnvironment{minted}{%
#+LATEX_HEADER: \end{mdframed}}
#+LATEX_HEADER: %% Formatting of verbatim outputs (i.e., outputs of R results):
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{%
#+LATEX_HEADER:   fontsize = \small,
#+LATEX_HEADER:   frame = leftline,
#+LATEX_HEADER:   formatcom = {\color{gray!97}}
#+LATEX_HEADER: }
#+LATEX_HEADER: \usepackage{float}
#+LATEX_HEADER: \usepackage{url}
#+LATEX_HEADER: %% For DOI hyperlinks in biblio:
#+LATEX_HEADER: \usepackage{doi}
#+LATEX_HEADER: \renewcommand{\doiprefix}{}
#+LATEX_HEADER: \usepackage{enumitem}
#+LATEX_HEADER: \setenumerate[1]{label=\emph{(\roman*}), resume}
#+LANGUAGE: fr

#+ATTR_LATEX: :width 0.12 \textwidth
[[./R_logo.png]]

* Description des données
  :PROPERTIES:
  :UNNUMBERED: t
  :END:
  On se propose ici de travailler sur un jeu de données
  anthropométrique disponible en libre accès sur Internet : le
  "Goldman Data Set". Ce jeu de données est disponible sur le site de
  Benjamin Auerbach (http://web.utk.edu/~auerbach/GOLD.htm). Il inclut
  des mesures osseuses de 1538 individus couvrant des périodes allant
  de $-6000$ ans jusqu'à nos jours, et provenant de tous les
  continents
  citep:auerbach2004_HumanBodyMass,auerbach2006_LimbBoneBilateral. Le
  détail complet des variables du jeu de données est disponible sur la
  page web.

* Inspection, importation et contrôle des données
  1. Télécharger le fichier de données dans le format qui vous semble
     approprié, puis l'inspecter à l'aide d'un tableur ou d'un éditeur
     de texte pour en connaître les détails de mise en forme
     (séparateur décimal, séparateur de champ, indicateur de données
     manquantes, etc.).
  2. Importer le fichier avec R en utilisant la fonction adaptée au
     format de fichier que vous avez choisi. Remarques :
     - vous risquez d'éprouver des difficultés pour spécifier le
       nom des individus, car aucune colonne du jeu de données
       n'est assimilable à un identifiant unique pour les
       individus : mieux vaut donc ne pas en spécifier tout de
       suite ;
     - l'encodage du fichier est un encodage ~macintosh~. Parcourir
       l'aide pour savoir comment l'indiquer à R.
  3. Afficher quelques résumés et effectuer quelques contrôles
     post-importation[fn::Il y en a au moins un qui est assez trivial
     ici : vous avez l'avantage de connaître le nombre de lignes que
     le fichier est supposé avoir.].

* Filtrage et mise en forme des données
  1. Le fichier comporte de très nombreuses variables et nous ne les
     utiliserons pas toutes. À l'aide de la syntaxe R de base ou d'une
     fonction du package ~dplyr~, retenir uniquement les variables
     suivantes du data frame : ~ID, Sex, NOTE, LHML, LHHD, LHAPD,
     LHMLD, LFML, LFHD, LRMLD, LRAPD~.
  2. De même, le fichier comporte beaucoup de populations différentes,
     et nous allons nous concentrer uniquement sur trois populations
     précises : la population de ~Cliff Dweller~ (USA, 800--600 BP),
     la population autrichienne médiévale de ~Hainburg~, et la
     population ancienne de chasseurs-cueilleurs de ~Indian Knoll~
     (USA, 5500--3700 BP). Filtrer le jeu de données pour ne retenir
     que ces trois populations (indiquées par la colonne
     ~NOTE~). Combien d'individus reste-t-il désormais ?
  3. La variable ~Sexe~ est codée de façon peu lisible en
     l'état. Recoder ce facteur pour transformer les ~0~ en ~Male~ ;
     les ~1~ en ~Female~ ; et transformer les individus dont le sexe
     est incertain (~0?~ ou ~1?~) en valeurs manquantes ~NA~. Il y a
     plusieurs possibilités pour cela (cf. l'aide et/ou votre moteur
     de recherche favori), dont :
     - la fonction ~levels()~, fonction de base de R ;
     - la fonction ~fct_recode()~ du package ~forcats~.
  4. Appliquer un nouveau ~summary()~ : dans le facteur ~NOTE~,
     certains niveaux de facteurs désormais inutilisés sont toujours
     présents en mémoire. Bien que ce ne soit pas toujours dérangeant,
     la fonction ~droplevels()~ permet de les éliminer (consulter son
     aide).

* Analyse de la structure de corrélation
  1. Calculer et afficher la matrice de corrélations pour l'ensemble
     des variables numériques du data frame[fn::D'une manière ou d'une
     autre, il vous faudra au préalable trouver un moyen de ne retenir
     que les variables numériques du dataframe.]. Détectez-vous "à
     l'oeil nu" quelque chose de particulier dans la structure de
     corrélation des variables ?
  2. Pour aller plus loin, installer (si nécessaire) et charger le
     package ~corrplot~. Consulter sa vignette[fn::La \emph{vignette}
     d'un package est un descriptif pédagogique et illustré des
     fonctionnalités de ce package. De nombreux packages populaires ou
     \og bien maintenus\fg{} en possèdent désormais une.] :
     https://cran.r-project.org/web/packages/corrplot/vignettes/corrplot-intro.html. À
     l'aide des informations récoltées sur cette vignette, tenter de
     mettre en évidence une structure de corrélation par blocs dans
     ces variables numériques ; puis la commenter.
  3. Parmi les paires de variables bien corrélées, il y a la paire
     ~(LFML, LHML)~. Ou bien à l'aide d'une fonction issue du package
     ~lattice~ (relativement simple), ou bien à l'aide d'une
     combinaison de fonctions R de base (plus compliqué !),
     représenter sur une même fenêtre graphique trois nuages de
     points. Ils représenteront la liaison ~(LFML, LHML)~ au sein des
     trois populations étudiées. Sur chaque nuage, l'information
     ~Sexe~ sera mise en évidence par la couleur des points
     représentés. En résumé, on aura donc un nuage par population, et
     une couleur par sexe sur chaque nuage.

* Dimorphisme sexuel pour le fémur à Indian Knoll
  1. Créer un nouveau data frame nommé ~ik~, qui sera constitué
     uniquement des individus complets en provenance de la population
     d'Indian Knoll. Combien de femmes et d'hommes reste-t-il ?
  2. Créer dans ce data frame une nouvelle colonne nommée ~Ratio~, qui
     sera égale au rapport ~LFML / LFHD~.
  3. Est-ce que ce ratio ainsi créé peut être considéré comme suivant
     une loi normale à la fois pour les femmes et pour les hommes ?
     Pour répondre à cette question, on pourra combiner des
     représentations graphiques (indispensables) avec des tests de
     normalité (plus facultatifs).
  4. Représenter des boites de dispersion en parallèle pour ce ratio
     en fonction du sexe. Semble-t-il y avoir des différences ?
     Procéder à un test adéquat pour mettre en évidence ces
     différences, et commenter également l'intervalle de confiance de
     la différence entre les deux moyennes.

* Questions bonus diverses
  1. Qui est l'individu ayant la valeur maximale pour la variable
     ~LHML~ ? Il y aura au moins trois démarches possibles pour
     répondre à cette question.
  2. Il sera souvent utile de définir des fonctions personnelles en R,
     car tous vos besoins ne seront pas forcément déjà implémentés
     dans le logiciel ! Voici par exemple comment créer et définir une
     fonction personnelle qui calcule le coefficient de variation d'un
     vecteur ~x~ donné, c'est-à-dire le rapport de son
     écart-type et de sa moyenne :
     #+begin_src R :results silent :exports code :eval no
my_cv <- function(x, na.rm = TRUE) {
    valeur <- sd(x, na.rm = na.rm) / mean(x, na.rm = na.rm)
    return(valeur)
}
     #+end_src
     Utiliser cette fonction pour calculer le coefficient de variation
     de ~LHML~, puis de toutes les variables numériques du jeu de
     données (la fonction ~apply()~ aidera beaucoup !).

* Biblio                                                             :ignore:
bibliography:~/PACEA_MyCore/complete_biblio.bib
bibliographystyle:apacite


